# SKA Tango Alarmhandler

This project provides alarm handler solution for SKA. The alarm handler solution is based on Elettra Alarm Handler provided by Tango community. The repository contains set of Dockerfiles, helm charts and scripts that are useful for building, deploying and configuring the Elettra Alarm Handler.


## Getting started

## Build

To build docker image of Elettra Alarm Handler, go to terminal and enter below command:

```commandline
    make oci-image-build
```

## Testing

```commandline
    make k8s-test
```

### To deploy the alarm handler enter following command:

```commandline
    make k8s-install-chart
```

The alarm handler gets deployed in namespace `ska-tango-alarmhandler`

### To delete the deployment enter following command:

```commandline
    make k8s-uninstall-chart
```

### Useful commands

Command to wait till archiver deployment is complete:

```commandline
    make k8s-wait
```

Command to delete the namespace:

```commandline
    make k8s-delete-namespace
```

## Configuring the alarms

### Steps to deploy locally and access alarm handler configurator tool.

1. Once the deployment is completed, execute ``minikube service alarm-handler-configurator -n <KUBE_NAMESPACE> --url`` to get the url of alarm handler configurator tool.
2. Click on the below link, then the alarm handler configurator tool UI as shown below will appear.

    ![Alarm Handler](/docs/src/configurator-UI.png "Alarm Handler Configurator UI")

### Steps to access alarm handler configurator tool in STFC cluster.
1. Deploy the alarm handler chart
2. Alarm configurator tool can access using https://k8s.stfc.skao.int/$(KUBE_NAMESPACE)/alarm-configurator/

### UI DETAILS:
The UI consists of:
1. Add Alarms box:
    i. File Upload box : This option helps to upload the user's alarm rules text file. Only text files are allowed to be uploaded.
    ii. Input text: This text accepts alarm handler Tango Resource Locator(TRL) which will be used to provide current alarms configuration summary.
2. Remove Alarm box:
    ii. Input text: This text accepts alarm tag from user.
    ii. Input text: This text accepts alarm handler Tango Resource Locator(TRL) which will be used to provide current alarms configuration summary.
6. Refresh Table button : Once clicked will sync table with latest operations performed with other clients.
7. Table : This will be visible once you perform any operation like add-alarms/remove-alarm. The table consists of Alarm Tag, Device name, Attribute name, Alarm State and Alarm message


### HOW TO CONFIGURE ALARMS:

1. User can upload the text file containing alarm rules and choose the option [add-alarms].
2. Once submitted , user will able to see table as shown below containing summary of alarms configured in the system.

    ![Alarm Handler](docs/src/configurator-UI.png "Alarm Handler Configurator UI after configuration")


### API DETAILS:

1. [POST]add-alarms

      This API helps to add alarms to configure using text file and Tango Resource Locator(TRL) for Alarm Handler device.

      | http verb allowed : ``POST``
      | Content-Type: ``multipart/form-data``
      | header: ``accept:application/json``

      .. code-block::

            curl -X 'POST' \
            'http://<IP>:8004/<KUBE NAMESPACE>/add-alarms' \
            -H 'accept: application/json' \
            -H 'Content-Type: multipart/form-data' \
            -F 'file=@alarm_rules.txt;type=text/plain' \
            -F 'trl=<alarm handler trl>'
  

2. [POST]remove-alarm

      This API helps to remove alarms using text file and Tango Resource Locator(TRL) for Alarm Handler device.

      | http verb allowed : ``POST``
      | Content-Type: ``multipart/form-data``
      | header: ``accept:application/json``

      .. code-block::

            curl -X 'POST' \
            'http://<IP>:8004/<KUBE NAMESPACE>/remove-alarm' \
            -H 'accept: application/json' \
            -H 'Content-Type: multipart/form-data' \
            -F 'tag=<alarm tag to remove>' \
            -F 'alarm_handler_trl=<alarm handler trl>'
    

3. [GET]alarm-summary

      This API provides the updated alarm summary present for the configured alarms

      | http verb allowed : ``GET``
      | header: ``accept:application/json``

      .. code-block::

            curl -X 'GET' \
            'http://<IP>:8004/<KUBE NAMESPACE>/alarm-summary' \
            -H 'accept: application/json'