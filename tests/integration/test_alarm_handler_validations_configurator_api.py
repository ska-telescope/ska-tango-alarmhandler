"""
This module is used for testing validations added for Alarm-Handler configurator API.
"""
import httpx
import pytest
from conftest import CLUSTER_DOMAIN, KUBE_NAMESPACE, get_http_url


def alarm_rule_validation(filename, missing_attribute):
    """Test method to verify validation against alarm rules"""
    with open(f"/app/tests/resources/alarm_files/{filename}", "rb") as file:
        response = httpx.post(
            get_http_url(
                kube_namespace=KUBE_NAMESPACE,
                cluster_domain=CLUSTER_DOMAIN,
                port=8004,
                api_operation="add-alarms",
            ),
            files={"file": (filename, file, "text/plain")},
            data={"trl": "alarm/handler/01"},
        )
        response_data = response.json()
        assert f"Missing {missing_attribute} property in alarm rule" in response_data["error"]


def alarm_rule_file_format(filename):
    """Test method to verify file format for the alarm rules"""

    with open(f"/app/tests/resources/alarm_files/{filename}", "rb") as file:
        response = httpx.post(
            get_http_url(
                kube_namespace=KUBE_NAMESPACE,
                cluster_domain=CLUSTER_DOMAIN,
                port=8004,
                api_operation="add-alarms",
            ),
            files={"file": (filename, file, "text/plain")},
            data={"trl": "alarm/handler/01"},
        )
        response_data = response.json()
        assert "Provided file is not .txt file" in response_data["error"]


@pytest.mark.parametrize(
    "alarm_rule_file, missing_attribute",
    [
        ("missing_tag_attribute.txt", "tag"),
        ("missing_formula_attribute.txt", "formula"),
        ("missing_priority_attribute.txt", "priority"),
        ("missing_group_attribute.txt", "group"),
        ("missing_message_attribute.txt", "message"),
    ],
)
@pytest.mark.post_deployment
@pytest.mark.SKA_mid
def test_validate_attribute_properties(alarm_rule_file, missing_attribute):
    """test case to validate alarm attribute properties for mid"""
    alarm_rule_validation(alarm_rule_file, missing_attribute)


@pytest.mark.post_deployment
@pytest.mark.SKA_mid
def test_alarm_rule_file_format():
    """test case to configure alarms for mid"""
    alarm_rule_file_format("not_allowed_format.txts")
