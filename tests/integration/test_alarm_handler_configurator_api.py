"""
This module is used for testing Alarm-Handler configurator API.
"""
import httpx
import pytest
from conftest import CLUSTER_DOMAIN, KUBE_NAMESPACE, get_http_url


def add_alarms_api(filename):
    """Test method for add alarms API"""
    with open(f"/app/tests/resources/alarm_files/{filename}", "rb") as file:
        response = httpx.post(
            get_http_url(
                kube_namespace=KUBE_NAMESPACE,
                cluster_domain=CLUSTER_DOMAIN,
                port=8004,
                api_operation="add-alarms",
            ),
            files={"file": (filename, file, "text/plain")},
            data={"trl": "alarm/handler/01"},
        )
        response_data = response.json()
        assert response_data["alarm_summary"]["tag"] == ["test", "test1"]


def remove_alarm_api():
    """Test method for remove alarms API"""
    tags_to_remove = ["test", "test1"]
    for tag in tags_to_remove:
        response = httpx.post(
            get_http_url(
                kube_namespace=KUBE_NAMESPACE,
                cluster_domain=CLUSTER_DOMAIN,
                port=8004,
                api_operation="remove-alarm",
                tag_to_remove=tag,
            ),
            data={"tag": f"{tag}", "alarm_handler_trl": "alarm/handler/01"},
        )
        response_data = response.json()
    assert response_data["alarm_summary"] is None


@pytest.mark.post_deployment
@pytest.mark.SKA_mid
def test_configure_alarms():
    """test case to configure alarms for mid"""
    add_alarms_api("alarm_rules.txt")


@pytest.mark.post_deployment
@pytest.mark.SKA_mid
def test_remove_alarm():
    """test case to remove alarm for mid"""
    remove_alarm_api()
