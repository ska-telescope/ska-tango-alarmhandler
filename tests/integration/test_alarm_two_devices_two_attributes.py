"""
This module is used for testing Alarm-Handler configurator API
for two different attributes for different devices.
"""
import httpx
import pytest
from conftest import CLUSTER_DOMAIN, KUBE_NAMESPACE, get_http_url, tear_down


def add_alarms_with_two_devices(filename):
    """Test method for add alarms API"""
    with open(f"/app/tests/resources/alarm_files/{filename}", "rb") as file:
        response = httpx.post(
            get_http_url(
                kube_namespace=KUBE_NAMESPACE,
                cluster_domain=CLUSTER_DOMAIN,
                port=8004,
                api_operation="add-alarms",
            ),
            files={"file": (filename, file, "text/plain")},
            data={"trl": "alarm/handler/01"},
        )
        response_data = response.json()
        assert response_data["alarm_summary"]["tag"] == ["dummyalarm1"]
        assert response_data["alarm_summary"]["formula"] == [
            "(test/power_supply/1/current == 8.4 && test/power_supply/2/voltage > 5)"
        ]
    tear_down(["dummyalarm1"])


@pytest.mark.post_deployment
@pytest.mark.SKA_mid
def test_configure_alarms_two_devices():
    """test case to configure alarms for mid"""
    add_alarms_with_two_devices("complex_rule_with_two_devices.txt")
