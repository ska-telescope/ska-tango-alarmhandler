"""Test configuration file for ska-tango-alarmhandler"""
import os

import httpx

KUBE_NAMESPACE = os.getenv("KUBE_NAMESPACE")
CLUSTER_DOMAIN = os.getenv("CLUSTER_DOMAIN")


def get_http_url(
    kube_namespace: str,
    cluster_domain: str,
    port: str,
    api_operation: str,
    tag_to_remove: str = "",
):
    """Method to get URL
    :param kube_namespace: Kubenamesapce
    :type kube_namespace: str
    :param cluster_domain: Cluster Domain
    :type cluster_domain: str
    :param port: exposed port
    :type port: str
    :param api_operation: Requested API operation
    :type api_operation: str
    :param tag_to_remove: Alarm tag to remove
    :type tag_to_remove: str

    return:
        URL string for requested API operation

    rtype: str
    """
    if tag_to_remove:
        url = (
            f"http://alarm-handler-configurator.{kube_namespace}.svc."
            + f"{cluster_domain}:{port}/{api_operation}?tag={tag_to_remove}"
            + "&alarm_handler_trl=alarm%2Fhandler%2F01"
        )
    else:
        url = (
            f"http://alarm-handler-configurator.{kube_namespace}.svc."
            + f"{cluster_domain}:{port}/{api_operation}?trl=alarm%2Fhandler%2F01"
        )
    return url


def tear_down(tags_to_remove: list):
    """Test method for remove alarms API
    :param tags_to_remove: Alarm tag list to remove
    :type tags_to_remove: list
    """
    for tag in tags_to_remove:
        response = httpx.post(
            get_http_url(
                kube_namespace=KUBE_NAMESPACE,
                cluster_domain=CLUSTER_DOMAIN,
                port=8004,
                api_operation="remove-alarm",
                tag_to_remove=tag,
            ),
            data={"tag": f"{tag}", "alarm_handler_trl": "alarm/handler/01"},
        )
        response_data = response.json()
    assert response_data["alarm_summary"] is None
