ARG BUILD_IMAGE="artefact.skao.int/ska-tango-images-pytango-builder:9.3.32"
ARG BASE_IMAGE="artefact.skao.int/ska-tango-images-pytango-runtime:9.3.19"
FROM $BUILD_IMAGE AS buildenv

FROM $BASE_IMAGE

USER root

RUN apt-get update && \
    apt-get -y install python3-pip && \
    apt-get -y install libboost-all-dev && \
    apt-get install -y ca-certificates  && \
    apt-get install -y gnupg && \
    apt-key adv --keyserver hkp://keyserver.ubuntu.com:80 --recv-keys 0E98404D386FA1D9 648ACFD622F3D138 && \
    apt-get update && \
    apt-get install -y git && \
    apt-get install -y cmake && \
    apt-get install -y pkg-config

# Copy the necessary files
COPY alarm-handler alarm-handler
COPY makefiles makefiles

RUN cd alarm-handler \
 && mkdir build \
 && cd build \
 && export PKG_CONFIG_PATH=/non/standard/tango/install/location \
 && cmake .. -DCMAKE_PREFIX_PATH=/non/standard/tango/install/location \
 && make \
 && make install

EXPOSE 8004

RUN pip install fastapi && pip install "uvicorn[standard]" \
&& pip install jinja2 \
&& pip install python-multipart \
&& pip install -i https://artefact.skao.int/repository/pypi-internal/simple ska_ser_logging

# Cleanup: Remove the source code files
RUN rm -rf makefiles alarm-handler

USER tango