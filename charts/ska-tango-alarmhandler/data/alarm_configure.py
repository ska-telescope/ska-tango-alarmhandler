# Imports
import sys
import logging
import getopt
from tango import DeviceProxy, DevFailed
import time

logging.basicConfig(format='%(asctime)s - %(message)s', datefmt='%d-%b-%y %H:%M:%S', level=logging.INFO)

# to show alarm summary
def show_summary(success_count, error_count, errors, is_configured):
    logging.info(f"Configured successfully:{success_count}")
    logging.info(f"Failed:{error_count}")
    if is_configured:
        alarm_summary(dev_name)
    if len(errors) > 0:
        logging.error(f"Errors occured while loading alarms:{len(errors)}")
        for error in errors:
            logging.error(f"Error Description:{error.args[0].desc}")

def alarm_summary(dev_name):
    alarms_loaded = []
    alarm_summary = dev_name.alarmSummary
    for alarm_item in alarm_summary:
        alarm_tag = alarm_item.split(";")[0]
        alarms_loaded.append(alarm_tag)
    logging.info(f"Configured alarms list:{alarms_loaded}")

# parse arguments
try:
    opts, args = getopt.getopt(
        sys.argv[1:], "f:a", ["file=", "ah="]
    )

except getopt.GetoptError:
    logging.info("Please provide proper arguments. Usage: $python alarm_configure.py --f=<filepath> --a=<ALARM_HANDLER_NAME> file: File alarm rules ah: Alarm handler device name")
    sys.exit(2)

for opt, arg in opts:
    if opt in ("-f", "--file"):
        ALARM_RULE_FILE = arg
    elif opt in ("-a", "--ah"):
        alarm_handler_fqdn = arg


with open(ALARM_RULE_FILE, "r") as alarm_rule_file:
    alarm_configure_dict = {}
    alarm_configure_dict["configure_success_count"] = 0
    alarm_configure_dict["configure_fail_count"] = 0
    is_configured = None
    errors = []
    for line in alarm_rule_file:
        line = line.rstrip()
        dev_name = DeviceProxy(alarm_handler_fqdn)
        try:
            dev_name.command_inout("Load",line)
            time.sleep(0.1)
            alarm_configure_dict["configure_success_count"] +=1
            is_configured = True
            time.sleep(0.5)
        except DevFailed as e:
            alarm_configure_dict["configure_fail_count"] += 1
            errors.append(e)

show_summary(alarm_configure_dict["configure_success_count"], alarm_configure_dict["configure_fail_count"], errors, is_configured)
