# -*- coding: utf-8 -*-
"""
This is command line interface tool to view the alarms handled by Elettra Alarm Handler
"""
import getopt
import os
import sys
import time

from tabulate import tabulate
from tango import DeviceProxy

ALARM_DETAILS = {}

try:
    opts, args = getopt.getopt(
        sys.argv[1:], "a:", ["ah="]
    )
except getopt.GetoptError:
    print("Please provide proper arguments.")
    print("Usage: $python cli_viewer.py --a=<ALARM_HANDLER_FQDN>")
    print("$python cli_viewer.py -ah <ALARM_HANDLER_DEVICE>")
    print("ah: Alarm handler fqdn")
    sys.exit(2)

for opt, arg in opts:
    if opt in ("-a", "--ah"):
        alarm_handler_name = arg

alarm_device = DeviceProxy(alarm_handler_name)


def cli_viewer():
    """ Update ALARM_DETAILS dictionary with all the alarm details"""
    try:
        while True:
            alarm_summary_tuple = alarm_device.alarmSummary
            time.sleep(0.5)
            if alarm_summary_tuple == None:
                print("Alarm has not been raised.")
            else:
                for alarm_summary in alarm_summary_tuple:
                    attribute_details = {}
                    alarm_summary_string = "".join(alarm_summary)
                    alarm_summary_list = alarm_summary_string.split(";")
                    for item in alarm_summary_list:
                        if "time" in item:
                            time_value = item.split("=")[1]
                            attribute_details["time"] = time_value
                        if "formula" in item:
                            formula = item.split("=")[1]
                            split_by_space = formula.split()
                            attribute_name = (split_by_space[0].split("/"))[-1]
                            attribute_details["attribute"] = attribute_name
                            split_by_bracket = formula.split("(")
                            attribute_fqdn = (split_by_bracket[1].split())[0]
                            attribute_details["attribute_fqdn"] = attribute_fqdn
                            splitted_fqdn = attribute_fqdn.split("/")
                            splitted_fqdn.pop(-1)
                            device_fqdn = "/".join(splitted_fqdn)
                            attribute_details["device"] = device_fqdn
                        if "state=" in item:
                            state = item.split("=")[1]
                            if state in ["NORM", "UNACK", "ACKED", "RTNUN", "SHLVD", "DSUPR", "OOSRV","ERROR"]:
                                attribute_details["state"] = state
                        if "message" in item:
                            message = item.split("=")[1]
                            attribute_details["message"] = message
                        if "tag" in item:
                            tag = item.split("=")[1]
                            ALARM_DETAILS[tag] = attribute_details
                event_summary = alarm_device.eventSummary
                for item in ALARM_DETAILS:
                    attribute_fqdn = ALARM_DETAILS[item]["attribute_fqdn"]
                    for evt in event_summary:
                        if attribute_fqdn.lower() in evt:
                            splitted_evt_summary = evt.split(";")
                            for value_output in splitted_evt_summary:
                                if "values" in value_output:
                                    value = (value_output.split("="))[-1]
                                    ALARM_DETAILS[item]["value"] = value
            os.system('clear')
            create_table()
    except KeyboardInterrupt:
        print("Exited")


def create_table():
    """Function to create table for active alarms"""

    inactive_alarm_states = ["NORM", "RTNUN", "DSUPR", "OOSRV","ERROR"]
    if ALARM_DETAILS:
        for item in list(ALARM_DETAILS):
            if ALARM_DETAILS[item]['state'] in inactive_alarm_states:
                ALARM_DETAILS.pop(item)
    table = []
    for item in ALARM_DETAILS.items():
        alarm_list = []
        alarm_list.append(item[0])
        alarm_name = item[1]
        alarm_list.append(alarm_name['device'])
        alarm_list.append(alarm_name['attribute'])
        alarm_list.append(alarm_name['time'])
        alarm_list.append(alarm_name['value'])
        alarm_list.append(alarm_name["state"])
        alarm_list.append(alarm_name['message'])
        table.append(alarm_list)
    headers=["Alarm Name", "Device", "Attribute", "Time(UTC)", "Value", "State", "Message"]
    print(f"Alarm details for {alarm_handler_name}:")
    print("\n",tabulate(table, headers, tablefmt="grid"),"\n")
    print("\nTo exit, press ctrl+c")


cli_viewer()
