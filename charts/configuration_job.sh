FILE=charts/configuration_job.yaml
cat <<EOF > ${FILE}
apiVersion: batch/v1
kind: Job
metadata:
  name: alarm-configuration
spec:
  ttlSecondsAfterFinished: 10
  template:
    spec:
      volumes:
      - name: alarm-configuration
        configMap:
          name: "alarm-configure"
      containers:
      - name: alarm-configuration
        image: artefact.skao.int/ska-tango-images-tango-itango:9.3.9
        imagePullPolicy: IfNotPresent
        command:
          - sh
        args:
          - -c
          - "/usr/bin/python /app/data/alarm_configure.py --file=/app/data/$1 --ah=$2 "
        env:
        - name: TANGO_HOST
          value: $TANGO_HOST
        volumeMounts:
          - name: alarm-configuration
            mountPath: /app/data
            readOnly: true   
      restartPolicy: Never
EOF