import os
import pathlib
from fastapi import FastAPI, UploadFile, Request, Form
from fastapi.responses import HTMLResponse
from fastapi.staticfiles import StaticFiles
from fastapi.templating import Jinja2Templates
import logging
from tango import DeviceProxy, DevFailed
from ska_ser_logging import configure_logging

app = FastAPI()

ALARM_HANDLER_TRL = "alarm/handler/01"

configure_logging()
logger = logging.getLogger("alarm_configurator")

templates = Jinja2Templates(directory="templates")
app.mount("/static", StaticFiles(directory="static"), name="static")

@app.post('/add-alarms')
async def add_alarms(
        file: UploadFile, trl: str = Form()
):
    """Method for API to fetch file from the user and process it for adding and 
    configuring alarms

    Args:
        file (UploadFile): file uploaded from the user
        trl (str): alarm handler device instance trl
        ex - "alarm/handler/01" or "tango://host:port/name/of/device"

    Return:
        json: returns json response with processed output containing alarm summary and 
        alarms details with failed and successful configured alarms count.
        
    """
    try:
        file_extension = pathlib.Path(file.filename).suffix
        if file_extension == ".txt":
            text_binary = await read_txt(file)
            response = text_binary.decode()
            validate_attribute_properties(response)
            result = load_alarms(response, trl)
            return result
        else:
            logger.error(f"Provided file is not .txt file")
            result = {"error": f"Provided file is not .txt file"}
            return result
        
    except AlarmRuleValidationError as ex:
        return {"error": str(ex)}
    
    except Exception as ex:
        return {"error": str(ex)}
    


def read_txt(file: UploadFile):
    """ Method to read the text file uploaded from the user

    Arg: 
        file(UploadFile): file uploaded from user

    Return:
        file: text file

    """
    return file.read()


@app.post('/remove-alarm')
async def remove_alarm(tag: str = Form(), alarm_handler_trl: str = Form()):
    """Method for API to fetch tag string from the user 
    and remove the alarm with given tag from configured alarms

    Args:
        tag(str): alarm tag given by the user
        alarm_handler_trl (str): alarm handler device TRL instance 
        ex - "alarm/handler/01" or "tango://host:port/name/of/device"

    Return:
        json: json response with processed output containing
        message and updated alarm summary

    """
    result = remove_configured_alarm(tag, alarm_handler_trl)
    return result


@app.get("/", response_class=HTMLResponse)
def add_config(request: Request):
    """Method for API renders index page """
    return templates.TemplateResponse(
        "index-page.html",
        {"request": request}
    )

@app.get("/alarm-summary")
def get_alarm_summary():
    """
    API to get alarm summary of alarm configuration present in the system.
    """

    alarm_handler_proxy = get_device_proxy(ALARM_HANDLER_TRL)
    alarm_summary_dict = get_attribute_properties_summary(alarm_handler_proxy)
    logger.info(f"Updated alarm summary: {alarm_summary_dict}")
    return {"alarm_summary": alarm_summary_dict} 


def load_alarms(response: str, trl: str) -> dict:
    """
    Method to load alarms using alarmhandler Load API

    Args:
       response (str): alarm rules to load
       trl (str): alarm handler device instance TRL
       ex - "alarm/handler/01" or "tango://host:port/name/of/device"
    
    Return:
       json: returns json containing alarm summary and configured alarms details with failed 
       and successful configured alarms count.

    """
    alarm_handler_proxy = get_device_proxy(trl)
    alarm_configure_dict = {}
    configure_errors = []
    alarm_configure_dict["configure_success_count"] = 0
    alarm_configure_dict["configure_fail_count"] = 0
    alarm_rules_list = response.split("\n")
    for rule in alarm_rules_list:
        if rule:
            try:
                alarm_handler_proxy.command_inout("Load",rule)
                alarm_configure_dict["configure_success_count"] +=1
            except DevFailed as ex:
                logger.exception(f"Exception occured: {ex}")
                alarm_configure_dict["configure_fail_count"] += 1
                configure_errors.append(ex.args[0].desc)

    alarm_summary_dict = get_attribute_properties_summary(alarm_handler_proxy)
    logger.info(f"Alarm summary for the configured alarms: {alarm_summary_dict}")
    if configure_errors and alarm_summary_dict:
        return {"error": configure_errors,
            "alarm_summary": alarm_summary_dict,
            "configured_alarms": alarm_configure_dict }
    elif len(configure_errors) > 0:
        return {"error": configure_errors}
    else:
        return {"alarm_summary": alarm_summary_dict,
            "configured_alarms": alarm_configure_dict }
       

def remove_configured_alarm(alarm_tag: str, trl: str) -> dict:
    """
    Method to remove alarm with tag

    Args:
        alarm_tag(str): Input string with alarm tag to remove
        trl (str): alarm handler device instance TRL
        ex - "alarm/handler/01" or "tango://host:port/name/of/device"
    
    Return:
        json: json response with processed output containing
        message and updated alarm summary

    """
    alarm_handler_proxy = get_device_proxy(trl)
    alarm_tag = alarm_tag.lower()
    try:
        alarm_handler_proxy.command_inout("Remove", alarm_tag)
        alarm_summary_dict = get_attribute_properties_summary(alarm_handler_proxy)
        logger.info(f"Alarm summary after remove: {alarm_summary_dict}")
    except DevFailed as ex:
        logger.exception(f"Exception occured: {ex}")
        return {"error": ex.args[0].desc}
    except Exception as ex:
        logger.exception(f"Exception occured: {ex}")
        return {"error": ex}

    return {"message": f"Alarm with tags {alarm_tag} is removed successfully",
            "alarm_summary": alarm_summary_dict}


def get_device_proxy(trl: str) -> DeviceProxy:
    """
    Method to get device proxy of Alarm Handler tango device

    Arg:
        trl(str): TRL instance for alarm handler
        ex - "alarm/handler/01" or "tango://host:port/name/of/device"

    Return:
        alarm_handler_proxy: device proxy object of alarm handler device

    """
    alarm_handler_proxy = DeviceProxy(trl)
    return alarm_handler_proxy


def get_attribute_properties_summary(alarm_handler_proxy: DeviceProxy) -> dict:
    """
    A method to get attribute properties details from alarmSummary attribute value

    Arg:
        alarm_handler_proxy: DeviceProxy object for AlarmHandler device
    
    Return:
        attribute_properties_details(dict): Dict representing details for attribute properties
    """
    attribute_properties_details:dict = {}
    tag_list:list = []
    formula_list: list = []
    state_list:list = []
    message_list:list = []
    alarm_summary_tuple = alarm_handler_proxy.read_attribute("alarmSummary").value
    if alarm_summary_tuple:
        for alarm_summary in alarm_summary_tuple:
            alarm_summary_string = "".join(alarm_summary)
            alarm_summary_list = alarm_summary_string.split(";")
            for item in alarm_summary_list:
                if "tag=" in item:
                    tag_list.append(item.split("=")[1])
                if "formula=" in item:
                    formula_property = item.replace("formula=", "")
                    formula_list.append(formula_property)
                if "state=" in item:
                    state_list.append(item.split("=")[1])
                if "message=" in item:
                    message_list.append(item.split("=")[1])
        attribute_properties_details["tag"] = tag_list
        attribute_properties_details["formula"] = formula_list
        attribute_properties_details["state"] = state_list
        attribute_properties_details["message"] = message_list
        return attribute_properties_details

class AlarmRuleValidationError(Exception):
    """
    A class to raise exception if mandatory attribute properties
    are missing in provided alarm rules
    """
    pass


def validate_attribute_properties(response: str):
    """
    A method to validate the required attribute properties in alarm rule

    Arg:
        response(str): alarm rules to configure
    Raises:
        AlarmRuleValidationError: if mamndatory property fiels are 
        missing in alarm rule
    """
    required_properties = ["tag=", "formula=", "priority=", "group=", "message="]
    alarm_rules_list = response.split("\n")
    for rule in alarm_rules_list:
        if rule:
            for property in required_properties:
                if property not in rule:
                    raise AlarmRuleValidationError(f"Missing {property[:-1]} property in alarm rule")
                else:
                    logger.info("Alarm rule validation is successful")