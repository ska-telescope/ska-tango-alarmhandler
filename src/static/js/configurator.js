var base_url = window.location.href

$(function() {
    $("#uploadform").on("submit", function(event) {

        event.preventDefault();
        var files = $('#file')[0].files;
        var fd = new FormData();

        fd.append('file', files[0]);

        var trl = document.getElementById("trl").value;
        fd.append('trl',trl)
        var url = "add-alarms";
        $.ajax({
            url: base_url + url,
            type: 'post',
            dataType: 'json',
            processData: false,
            contentType: false,
            data: fd,
            success: function(data) {
                if (data["alarm_summary"] !== null && data["alarm_summary"] !== '' && data["alarm_summary"] !== undefined && data["error"] !== null && data["error"] !== '' && data["error"] !== undefined) {
                    $('#add-alarm-result').empty();
                    $('#operations tbody').empty();
                    $(".add-alarm-result").html(data["error"]);
                    displaydata(data)
                }
                else if (data["error"] !== null && data["error"] !== '' && data["error"] !== undefined) {
                    console.log(data)
                    $(".add-alarm-result").html(data["error"]);
                }
                else {
                    $('#add-alarm-result').empty();
                    $('#operations tbody').empty();
                    displaydata(data)
                }
            },
            error: function(data) {
                var error = JSON.stringify(data);
                alert(error)
            },
            complete: function(data) {
                $('#uploadform')[0].reset();
            }
        });
    });
})


$(function() {
    $("#removealarm").on("submit", function(event) {

        event.preventDefault();
        var fd = new FormData();
        var tag = document.getElementById("tag").value;
        fd.append('tag',tag)
        var alarm_handler_trl = document.getElementById("alarm_handler_trl").value;
        fd.append('alarm_handler_trl',alarm_handler_trl)
        var url = "remove-alarm";
        $.ajax({
            url: base_url + url,
            type: 'post',
            dataType: 'json',
            processData: false,
            contentType: false,
            data: fd,
            success: function(data) {
                if (data["error"] !== null && data["error"] !== '' && data["error"] !== undefined) {
                    $(".remove-alarm-result").html(data["error"]);
                    displaydata(data)
                } else {
                    $('#remove-alarm-result').empty();
                    $('#operations tbody').empty();
                    displaydata(data)
                }
            },
            error: function(data) {
                var error = JSON.stringify(data);
                alert(error)
            },
            complete: function(data) {
                $('#removealarm')[0].reset();
            }
        });
    });
})


function refreshtable() {
    $('#operations tbody').empty();
    getalarmsummary()
}
$(document).ready(
    getalarmsummary()
);

function getalarmsummary() {
    var url =  base_url+ "alarm-summary";
    $.ajax({
        url: url
    }).then(function(data) {
        displaydata(data)
    }
    )
}

async function displaydata(resp) {
    var table = document.getElementById("operations");
    var rowCount = table.rows.length;
    if ((rowCount) == 0) {
        var header = table.createTHead();
        var row = header.insertRow(0);
        var cell = row.insertCell(0);
        cell.innerHTML = "TAG"
        var cell = row.insertCell(1);
        cell.innerHTML = "TRIGGER CONDITION RULE"
        var cell = row.insertCell(2);
        cell.innerHTML = "STATE"
        var cell = row.insertCell(3);
        cell.innerHTML = "ALARM MESSAGE"
        $('<button onclick="refreshtable()">Refresh Table</button>').appendTo('#refreshtable');
    }
    var tbody = $('#operations tbody')[0];
    var taglist = resp["alarm_summary"]["tag"]
    if (typeof taglist != "undefined"){
        for (let i = 0; i < resp["alarm_summary"]["tag"].length; i++) {
            var tag = resp["alarm_summary"]["tag"][i]
            var formula = resp["alarm_summary"]["formula"][i]
            var state = resp["alarm_summary"]["state"][i]
            var message = resp["alarm_summary"]["message"][i]
            var rowCount = tbody.rows.length;
            var row = tbody.insertRow(rowCount);
            var cell = row.insertCell(0);
            cell.innerHTML = tag
            var cell = row.insertCell(1);
            cell.innerHTML = formula       
            var cell = row.insertCell(2)
            cell.innerHTML = state
            var cell = row.insertCell(3)
            cell.innerHTML = message           
        }
    }
}