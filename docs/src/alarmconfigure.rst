Configure Alarm Handler
=======================

Configuring alarm rules using txt file
--------------------------------------
* Steps to Configure alarm-handler.
      1. Clone ska-tango-alarmhandler repository : https://gitlab.com/ska-telescope/ska-tango-alarmhandler.git 
      2. ``cd ska-tango-alarmhandler``
      3. Download KUBECONFIG file.
      4. Run following make target with suitable variables.
  
.. code-block::

        make configure_alarm KUBECONFIG=<kubeconfig file> FILE_NAME=<.txt file with alarm rules set by user> ALARM_HANDLER_TRL=<Alarm handler device trl>
        KUBE_NAMESPACE=<namespace where alarm-handler is deployed>

**NOTE:** *FILE_NAME requires text file*