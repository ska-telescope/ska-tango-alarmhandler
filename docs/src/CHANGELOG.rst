###########
Change Log
###########

All notable changes to this project will be documented in this file.
This project adheres to `Semantic Versioning <http://semver.org/>`_.

Added
---------
[0.5.0]
*********
* REL-1921: Update ska-tango-alarmhandler utilising ska-tango-base v0.4.14 & ska-tango-utils v0.4.14 SKB-644 fix

[0.4.1]
*********
* REL-1482: Utilise elettra alarm handler with SKB-310 fix

[0.4.0]
*********
* Link to user documentation (Confluence or otherwise) in the Alarm Configurator web GUI
* Change table column Heading "FORMULA" to "TRIGGER CONDITION RULE"
* Replace all references from "FQDN" to "Tango Resource Locator (TRL)"
* Parse newline in alarm rule text file

[0.3.4]
*********
* Changes in alarm API scripts to handle complex alarm rules
* Improvements in alarm configurator tool UI (SP-3833)

[0.3.3]
*********
* Improvements in add-alarms API to configure alarm on multiple attributes for same device. 
* Added ingress to access alarm handler configurator tool from stfc cluster.

[0.3.2]
*********
* Implemented get api to fetch state of configured alarms

[0.3.1]
*********
* Develop UI for alarm handler configurator tool
* Modify FAST APIs created  to add-alarms and remove-alarm

[0.3.0]
*********
* Developed alarm configurator tool for Alarm Handler.
* Implemented API add-alarms to add alarms using FAST API.
* Implemented API remove-alarms to remove alarms using FAST API.

[0.2.0]
*********
* Updated tango base version to 0.4.4
* Update tango util version to 0.4.5

[0.1.5]
*********
* Removed alarms with ERROR state from display of cli viewer.

[0.1.4]
*********
* Updated command line interface tool
* Fixed  state display issue  in command line interface.
* Fixed tabulate dependency issue with higher itango versions

[0.1.3]
*********
* Improvements suggested by Feature Owner.

[0.1.2]
*********
* Provided simple command line interface tool to view the alarms handled by Elettra Alarm Handler.

[0.1.1]
*********
* Provide configure-script and make configure-alarm target for configuring the alarms. Intermediate chart release.

[0.1.0]
*********
* Provide Elettra as Alarm Handler Solution for SKAO. 
* Provide oci-image and chart with Elettra Alarm Handler for integration.