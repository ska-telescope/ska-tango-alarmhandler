API
===


The REST based API's can be used to configure alarm handler using curl command or other http clients.

Following are the API's provided by Alarm Handler configurator tool with examples showing details required by it.

1. [POST]add-alarms

      This API helps to add alarms to configure using text file and Tango Resource Locator(TRL) for Alarm Handler device.

      | http verb allowed : ``POST``
      | Content-Type: ``multipart/form-data``
      | header: ``accept:application/json``

      .. code-block::

            curl -X 'POST' \
            'http://<IP>:8004/<KUBE NAMESPACE>/add-alarms' \
            -H 'accept: application/json' \
            -H 'Content-Type: multipart/form-data' \
            -F 'file=@alarm_rules.txt;type=text/plain' \
            -F 'trl=<alarm handler trl>'
  

2. [POST]remove-alarm

      This API helps to remove alarms using text file and Tango Resource Locator(TRL) for Alarm Handler device.

      | http verb allowed : ``POST``
      | Content-Type: ``multipart/form-data``
      | header: ``accept:application/json``

      .. code-block::

            curl -X 'POST' \
            'http://<IP>:8004/<KUBE NAMESPACE>/remove-alarm' \
            -H 'accept: application/json' \
            -H 'Content-Type: multipart/form-data' \
            -F 'tag=<alarm tag to remove>' \
            -F 'alarm_handler_trl=<alarm handler trl>'
    

3. [GET]alarm-summary

      This API provides the updated alarm summary present for the configured alarms

      | http verb allowed : ``GET``
      | header: ``accept:application/json``

      .. code-block::

            curl -X 'GET' \
            'http://<IP>:8004/<KUBE NAMESPACE>/alarm-summary' \
            -H 'accept: application/json'