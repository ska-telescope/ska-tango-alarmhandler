Build and Test Commands
=======================

Build
--------

To build docker image of Elettra Alarm Handler , use following command:
    ``make oci-image-build``


Testing
-------
To execute integration tests on Alarm Handler, use following command:

    ``make k8s-test``


Useful commands
---------------

Command to wait till Alarm Handler deployment is complete:
    ``make k8s-wait``

Command to watch all the resources in the namespace:
    ``make k8s-watch``    

Command to delete the namespace:
    ``make k8s-delete-namespace``

