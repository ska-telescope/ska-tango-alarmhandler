AlarmHandler Deployment
***********************

* To deploy the Alarm Handler enter following command:
    
.. code-block::

        make k8s-install-chart

* The Alarm Handler gets deployed in namespace `ska-tango-alarmhandler`


* To delete the deployment enter following command:

.. code-block::

        make k8s-uninstall-chart
