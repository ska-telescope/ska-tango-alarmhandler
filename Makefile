CAR_OCI_REGISTRY_HOST:=artefact.skao.int
PROJECT = ska-tango-alarmhandler
KUBE_APP = ska-tango-alarmhandler
TELESCOPE ?= SKA-mid
PYTHON_SWITCHES_FOR_FLAKE8=
PYTHON_SWITCHES_FOR_PYLINT=
PYTHON_LINE_LENGTH = 99
PYTHON_LINT_TARGET ?= tests/ 
CI_PROJECT_PATH_SLUG ?= ska-tango-alarmhandler
CI_ENVIRONMENT_SLUG ?= ska-tango-alarmhandler
CLUSTER_DOMAIN ?= cluster.local

$(shell echo 'global:\n  annotations:\n    app.gitlab.com/app: $(CI_PROJECT_PATH_SLUG)\n    app.gitlab.com/env: $(CI_ENVIRONMENT_SLUG)' > gilab_values.yaml)
MARK ?= ## What -m opt to pass to pytest
FILE ?= tests## A specific test file to pass to pytest
ADD_ARGS ?= ## Additional args to pass to pytest

RELEASE_NAME ?= test## release name of the chart

TANGO_HOST ?=  tango-host-databaseds-from-makefile-$(RELEASE_NAME):10000## TANGO_HOST is an input!
# KUBE_NAMESPACE defines the Kubernetes Namespace that will be deployed to
# using Helm.  If this does not already exist it will be created
KUBE_NAMESPACE ?= ska-tango-alarmhandler
FILE_NAME?= alarm_rules.txt

# HELM_RELEASE is the release that all Kubernetes resources will be labelled
# with
HELM_RELEASE ?= test
HELM_CHARTS_TO_PUBLISH=ska-tango-alarmhandler

# UMBRELLA_CHART_PATH Path of the umbrella chart to work with
HELM_CHART=test-parent
UMBRELLA_CHART_PATH ?= charts/$(HELM_CHART)/
K8S_CHARTS ?= test-parent## list of charts
K8S_CHART ?= $(HELM_CHART)

CI_REGISTRY ?= gitlab.com
K8S_TEST_IMAGE_TO_TEST = artefact.skao.int/ska-tango-images-tango-itango:9.3.10 ## docker image that will be run for testing purpose
ifneq ($(CI_JOB_ID),)
CUSTOM_VALUES = --set ska-tango-alarmhandler.alarm_handler.image.image=$(PROJECT) \
	--set ska-tango-alarmhandler.alarm_handler.image.registry=$(CI_REGISTRY)/ska-telescope/$(PROJECT) \
	--set ska-tango-alarmhandler.alarm_handler.image.tag=$(VERSION)-dev.c$(CI_COMMIT_SHORT_SHA)
endif

CI_PROJECT_DIR ?= .

XAUTHORITY ?= $(HOME)/.Xauthority
THIS_HOST := $(shell ip a 2> /dev/null | sed -En 's/127.0.0.1//;s/.*inet (addr:)?(([0-9]*\.){3}[0-9]*).*/\2/p' | head -n1)
DISPLAY ?= $(THIS_HOST):0
JIVE ?= false# Enable jive
TARANTA ?= false
MINIKUBE ?= true ## Minikube or not
FAKE_DEVICES ?= true ## Install fake devices or not

ITANGO_DOCKER_IMAGE = $(CAR_OCI_REGISTRY_HOST)/ska-tango-images-tango-itango:9.3.12

# Test runner - run to completion job in K8s
# name of the pod running the k8s_tests
K8S_TEST_RUNNER = test-runner-$(HELM_RELEASE)

K8S_CHART_PARAMS = --set global.minikube=$(MINIKUBE) \
	--set global.tango_host=$(TANGO_HOST) \
	--set ska-tango-base.display=$(DISPLAY) \
	--set ska-tango-base.xauthority=$(XAUTHORITY) \
	--set ska-tango-base.jive.enabled=$(JIVE) \
	--set global.cluster_domain=$(CLUSTER_DOMAIN) \
	$(CUSTOM_VALUES)

PYTHON_VARS_BEFORE_PYTEST = PYTHONPATH=${PYTHONPATH}:/app:/app/tests KUBE_NAMESPACE=$(KUBE_NAMESPACE) HELM_RELEASE=$(RELEASE_NAME) TANGO_HOST=$(TANGO_HOST) CLUSTER_DOMAIN=$(CLUSTER_DOMAIN)

PYTHON_VARS_AFTER_PYTEST = --disable-pytest-warnings --timeout=300

configure_alarm:
	bash ./charts/configuration_job.sh $(FILE_NAME) $(ALARM_HANDLER_FQDN)
	kubectl  --kubeconfig=$(KUBECONFIG) create configmap alarm-configure  --from-file $(FILE_NAME) --from-file charts/ska-tango-alarmhandler/data/alarm_configure.py -o yaml -n $(KUBE_NAMESPACE) --dry-run=client | kubectl apply -f -
	kubectl  --kubeconfig=$(KUBECONFIG) create -f charts/configuration_job.yaml -n $(KUBE_NAMESPACE)
	kubectl  --kubeconfig=$(KUBECONFIG) wait --for=condition=Complete job/alarm-configuration -n $(KUBE_NAMESPACE)
	kubectl  --kubeconfig=$(KUBECONFIG) logs job.batch/alarm-configuration -n $(KUBE_NAMESPACE)
	rm charts/configuration_job.yaml

cred:
	make k8s-namespace
	curl -s https://gitlab.com/ska-telescope/templates-repository/-/raw/master/scripts/namespace_auth.sh | bash -s $(SERVICE_ACCOUNT) $(KUBE_NAMESPACE) || true

-include .make/k8s.mk
-include .make/python.mk
-include .make/helm.mk
-include .make/oci.mk
-include .make/base.mk
-include .make/xray.mk
-include PrivateRules.mak


.PHONY: help

test-requirements:
	@poetry export --without-hashes --dev --format requirements.txt --output tests/requirements.txt

k8s-pre-test: python-pre-test test-requirements

requirements: ## Install Dependencies
	poetry install



